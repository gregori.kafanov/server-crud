import express from 'express';

const app = express();

import userRouter from '../src/routes/users.js';

app.use(express.json());
app.use(
  express.urlencoded({
    extended: true,
  })
);

app.use('/users', userRouter);

export default app;
