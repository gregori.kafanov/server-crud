import pkg from "mongoose";
import mongoosePaginate from "mongoose-paginate-v2";
import { DEFAULT_AVATAR } from "../constants/index.js";
const { Schema, model } = pkg;

const UserSchema = new Schema(
  {
    firstName: { type: String, trim: true, required: true },
    lastName: { type: String, trim: true, required: true },
    avatar: { type: String, default: DEFAULT_AVATAR },
    email: { type: String, trim: true, required: true },
  },
  { timestamps: true, versionKey: false }
);

UserSchema.plugin(mongoosePaginate);
const UserModel = model("User", UserSchema);

export { UserModel };
