import mongoose from 'mongoose';
import dotenv from 'dotenv';
dotenv.config();

const DB_URI = process.env.DB_URI;

const connectDB = async () => {
  try {
    console.log('MongoDB: connecting ...');
    await mongoose.connect(`${DB_URI}`, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    });
    console.log('MongoDB: successfully connected');
  } catch (error) {
    console.log(error.message);
    process.exit(1);
  }
};

export default connectDB;
