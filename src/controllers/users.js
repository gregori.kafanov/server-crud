import { UserModel } from '../models/User.js';
import { createResponseStatus as status } from '../common/responseStatus.js';
class UserController {
  // GET
  async getAll(_, res) {
    const filter = {};

    try {
      const allUsers = await UserModel.find(filter);

      res.send({
        data: allUsers,
        ...status(200, false, 'Successful request'),
      });
    } catch (error) {
      res.send(status(404, true, error));
    }
  }

  async getAllPaginated(req, res) {
    const pagination = !!req.query.page;
    const options = {
      page: req.query.page,
      limit: req.query.limit,
      lean: true,
      pagination,
    };
    const filter = {};

    try {
      await UserModel.paginate(filter, options, (error, result) => {
        if (error) {
          throw Error(error);
        }

        if (!error) {
          res.send({
            data: {
              totalPages: result.totalPages,
              docs: result.docs,
              hasNextPage: result.hasNextPage,
              prevPage: result.prevPage,
            },
            ...status(200, false, 'Successful request'),
          });
        }
      });
    } catch (error) {
      res.send(status(404, true, error));
    }
  }

  async getOneById(req, res) {
    const { id: userId } = req.params;
    const filter = {
      _id: userId,
    };

    try {
      const foundOneUser = await UserModel.findOne(filter);

      res.send({
        data: foundOneUser,
        ...status(200, false, 'Successful request'),
      });
    } catch (error) {
      res.send(status(404, true, error));
    }
  }

  // POST
  async createOne(req, res) {
    const { body } = req;

    try {
      const createdUser = await UserModel.create(body);

      res.send({
        data: createdUser,
        ...status(200, false, 'Successful request'),
      });
    } catch (error) {
      res.send(status(404, true, error));
    }
  }

  // DELETE
  async deleteOneById(req, res) {
    const { id: userId } = req.params;
    const filter = {
      _id: userId,
    };

    try {
      const deletedUser = await UserModel.findOneAndRemove(filter);

      res.send({
        data: deletedUser,
        ...status(200, false, 'Successful request'),
      });
    } catch (error) {
      res.send(status(404, true, error));
    }
  }

  // PUT
  async updateOneById(req, res) {
    const { id: userId } = req.params;
    const updates = req.body;
    const filter = {
      _id: userId,
    };
    const updateOptions = { new: true };

    try {
      const beforeUpdate = await UserModel.findOneAndUpdate(
        filter,
        updates,
        updateOptions
      );

      res.send({
        data: beforeUpdate,
        ...status(200, false, 'Successfully updated'),
      });
    } catch (error) {
      res.send(status(404, true, error));
    }
  }
}

export { UserController };
