import dotenv from "dotenv";
dotenv.config();
import connectDB from "./db.js";
import app from "./app.js";

await connectDB();

const APP_PORT = process.env.PORT;

const server = app.listen(APP_PORT, function () {
  console.log(`Server running at http://localhost:${APP_PORT}`);
});

process.on("unhandledRejection", (err) => {
  console.log("UNHANDLED REJECTION! 💥 Shutting down...");
  console.log(err.name, err.message);
  server.close(() => {
    process.exit(1);
  });
});

process.on("SIGTERM", () => {
  console.log("👋 SIGTERM RECEIVED. Shutting down gracefully");
  server.close(() => {
    console.log("💥 Process terminated!");
  });
});
