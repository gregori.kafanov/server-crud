import express from "express";

const router = express.Router();

import { UserController } from "../controllers/users.js";

const userController = new UserController();

router.route("/paginated").get(userController.getAllPaginated);

router.route("/").get(userController.getAll).post(userController.createOne);

router
  .route("/:id")
  .get(userController.getOneById)
  .put(userController.updateOneById)
  .delete(userController.deleteOneById);

export default router;
