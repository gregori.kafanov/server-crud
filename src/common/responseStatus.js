export const createResponseStatus = (status, error, message) => {
  return {
    responseStatus: {
      status,
      error,
      message,
    },
  };
};
